{ pkgs ? import <nixpkgs> {} }:
let
  jobsets = {
    c-firmware = {
      enabled = 1;
      hidden = false;
      description = "card10 C firmware";
      nixexprinput = "rust-card10";
      nixexprpath = "hydra/firmware.nix";
      checkinterval = 300;
      schedulingshares = 100;
      enableemail = true;
      emailoverride = "astro@spaceboyz.net";
      keepnr = 3;
      inputs = {
        firmware = { type = "git"; value = "https://git.card10.badge.events.ccc.de/card10/firmware.git"; emailresponsible = false; };
        rust-card10 = { type = "git"; value = "https://git.card10.badge.events.ccc.de/astro/rust-card10.git"; emailresponsible = false; };
        nixpkgs = { type = "git"; value = "git://github.com/NixOS/nixpkgs.git release-19.03"; emailresponsible = false; };
      };
    };
    rust-l0dables = {
      enabled = 1;
      hidden = false;
      description = "card10 Rust l0dable examples";
      nixexprinput = "rust-card10";
      nixexprpath = "hydra/l0dables.nix";
      checkinterval = 300;
      schedulingshares = 100;
      enableemail = true;
      emailoverride = "astro@spaceboyz.net";
      keepnr = 3;
      inputs = {
        firmware = { type = "git"; value = "https://git.card10.badge.events.ccc.de/card10/firmware.git"; emailresponsible = false; };
        rust-card10 = { type = "git"; value = "https://git.card10.badge.events.ccc.de/astro/rust-card10.git"; emailresponsible = false; };
        nixpkgs = { type = "git"; value = "git://github.com/NixOS/nixpkgs.git release-19.03"; emailresponsible = false; };
        mozillaOverlay = { type = "git"; value = "git://github.com/mozilla/nixpkgs-mozilla.git"; emailresponsible = false; };
      };
    };
    firmware-combined = {
      enabled = 1;
      hidden = false;
      description = "Prepared firmware with Rust l0dables";
      nixexprinput = "rust-card10";
      nixexprpath = "hydra/combined.nix";
      checkinterval = 300;
      schedulingshares = 100;
      enableemail = true;
      emailoverride = "astro@spaceboyz.net";
      keepnr = 3;
      inputs = {
        rust-card10 = { type = "git"; value = "https://git.card10.badge.events.ccc.de/astro/rust-card10.git"; emailresponsible = false; };
        c-firmware = { type = "build"; value = "rust-card10:c-firmware:firmware"; emailresponsible = false; };
        rust-l0dables = { type = "build"; value = "rust-card10:rust-l0dables:l0dables"; emailresponsible = false; };
        nixpkgs = { type = "git"; value = "git://github.com/NixOS/nixpkgs.git release-19.03"; emailresponsible = false; };
      };
    };
  };

  jobsetsJson = pkgs.writeText "jobsets.json" (builtins.toJSON jobsets );
in
{
  jobsets = pkgs.runCommand "rust-card10.json" {} ''
    cp ${jobsetsJson} $out
  '';
}
