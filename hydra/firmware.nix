{ pkgs ? import <nixpkgs> {},
}:
with pkgs;

let
  firmwareSrc = import ../firmware.nix {
    inherit pkgs;
    src = <firmware>;
    srcPath = ".";
  };
  firmwareGit = firmwareSrc.override (oldArgs: {
    src = oldArgs.src.overrideAttrs (oldAttrs: {
      name = "${oldAttrs.name}-git";
      # no more git, .git is dropped by Hydra
      nativeBuildInputs = [];
      postPatch = ''
        VERSION="0.0-git"
        GITHASH="0000000000000000000000000000000000000000"

        substituteInPlace tools/version-header.sh \
          --replace "\$VERSION" "$VERSION" \
          --replace "\$GITHASH" "$GITHASH" \
          --replace "git -C" echo
      '';
    });
  });
  firmware = firmwareGit.firmware.overrideAttrs (oldAttrs: {
    buildCommand = ''
      ${oldAttrs.buildCommand}

      mkdir -p $out/nix-support
      for f in $out/**/*.elf $out/card10/*.bin ; do
        echo file binary-dist $f >> $out/nix-support/hydra-build-products
      done
    '';
  });
  firmwareZip = stdenv.mkDerivation {
    name = "card10-firmware.zip";
    nativeBuildInputs = [ firmware zip ];
    phases = [ "installPhase" ];
    installPhase = ''
      mkdir -p $out/nix-support

      cd ${firmware}/card10/
      zip -9r $out/firmware.zip .
      echo file binary-dist $out/firmware.zip >> $out/nix-support/hydra-build-products
    '';
  };
in {
  firmware = lib.hydraJob firmware;
  firmware-zip = lib.hydraJob firmwareZip;
}
